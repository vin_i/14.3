#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>


const int N = 124;

void PrintOddEvent(bool IsEven, int N)
{
	int Count;
	if (IsEven)
	{
        std::cout << "������ ����� �� 0 �� " << N << ": ";
        Count = 0;
    }
    else
    {
        std::cout << "�������� ����� �� 0 �� " << N << ": ";
        Count = 1;
    }

    while (Count <= N)
    {
        std::cout << Count << " ";
        Count += 2;
    }

    std::cout << "\n";
	
}

int main()

{
    setlocale(LC_CTYPE, "rus");

    std::cout << "������ ����� �� 0 �� " << ::N << ": ";
    for (int i = 0; i <= N; i += 2)
    {
        std::cout << i << " ";
    }
    std::cout << "\n";

    PrintOddEvent(true, N);
    PrintOddEvent(false, N);
}